package de.fhkiel.bi130.refactoring;

/**
 * Motherclass for Strategypattern.
 */
public abstract class NoiseBehavior
{
    @Override
    public abstract String toString();

    public abstract NoiseBehavior setNoise(String noise);
}
