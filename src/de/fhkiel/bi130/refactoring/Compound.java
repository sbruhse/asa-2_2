package de.fhkiel.bi130.refactoring;

import java.util.ArrayList;
import java.util.List;

/**
 * Compound is holding the animals.
 */
public class Compound
{
    private Habitat habitat;
    private List<Animal> animals = new ArrayList();

    public Compound(Habitat habitat)
    {
        this.habitat = habitat;
    }

    public boolean contains(Species species)
    {
        for (Animal animal : animals)
        {
            if (animal.getSpecies().getName() == species.getName())
            {
                return true;
            }
        }

        return false;
    }

    public void checkCompound()
    {
        for (Animal animal : animals)
        {
            animal.checkTemperature(this.habitat.getTemperature());
            if (!animal.getSpecies().getDefaultHabitat().toHabitat().equals(this.getHabitat()))
            {
                animal.setAlive(false);
            }

        }
    }

    public double getAveragePregnancyDuration()
    {
        double avgDuration = 0;
        if (!animals.isEmpty())
        {
            int sum = 0;
            for (Animal animal : animals)
            {
                sum += animal.getSpecies().getPregnancyDuration();
            }
            avgDuration = sum / (double) animals.size();
        }
        return avgDuration;
    }

    public int countAnimalsLegs()
    {
        int legSum = 0;
        for (Animal a : getAnimals()) legSum += a.getSpecies().getLegs();
        return legSum;
    }

    public void printCompoundInfo()
    {
        System.out.println("Here is some info about the " + this.getHabitat().getName() + ":");
        System.out.println("\tTemperature: " + this.getHabitat().getTemperature());
        System.out.println("\tAnimals: " + this.countAnimals());
        System.out.println("\tAverage Pregnancy Duration: " + this.getAveragePregnancyDuration());
    }


    public int countAnimals()
    {
        return animals.size();
    }

    public Habitat getHabitat()
    {
        return habitat;
    }

    public void setHabitat(Habitat habitat)
    {
        this.habitat = habitat;
    }

    public List<Animal> getAnimals()
    {
        return animals;
    }

    public void setAnimals(List<Animal> animals)
    {
        this.animals = animals;
    }

    public void addAnimal(Animal animal)
    {
        animals.add(animal);
    }


}
