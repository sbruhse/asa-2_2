package de.fhkiel.bi130.refactoring;

/**
 * Class for single Animal
 *
 */
public class Animal
{
    private String name = null;
    private Species species;
    private boolean alive = true;

    public Animal(Species species)
    {
        this.species = species;
    }

    public Animal(Species species, String name)
    {
        this.name = name;
        this.species = species;
    }

    public String getName()
    {
        if (name == null)
        {
            return "A " + getSpecies();
        }
        return this.name;
    }


    public void checkTemperature(int newTemperature)
    {
        if (getSpecies().getMinTemperature() > newTemperature || getSpecies().getMaxTemperature() < newTemperature)
        {
            this.setAlive(false);
        }

    }

    public void setAlive(boolean alive)
    {
        if (this.alive)
        {
            this.alive = alive;
        }
    }

    public boolean getAlive()
    {
        return alive;
    }


    public Species getSpecies()
    {
        return species;
    }

    public void setSpecies(Species species)
    {
        this.species = species;
    }
}
