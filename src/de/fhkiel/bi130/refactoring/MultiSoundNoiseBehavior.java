package de.fhkiel.bi130.refactoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Strategy for Animals which are doing more than one sound
 */
public class MultiSoundNoiseBehavior extends NoiseBehavior
{

    private List<String> noises;
    private Random random;


    public MultiSoundNoiseBehavior()
    {
        random = new Random();
        noises = new ArrayList<>();
    }

    @Override
    public String toString()
    {
        return noises.get(random.nextInt(noises.size()));
    }

    @Override
    public NoiseBehavior setNoise(String noise)
    {
        this.noises.add(noise);
        return this;
    }
}
