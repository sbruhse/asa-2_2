package de.fhkiel.bi130.refactoring;

/**
 * Builder for Species
 */
public class SpeciesBuilder
{
    private String name;
    private int speed;
    private int minTemperature;
    private int maxTemperature;
    private Habitat.Type defaultHabitat;
    private NoiseBehavior noiseBehavior;
    private int pregnancyDuration = 0;
    private int legs = 0;

    public SpeciesBuilder setName(String name)
    {
        this.name = name;
        return this;
    }

    public SpeciesBuilder setSpeed(int speed)
    {
        this.speed = speed;
        return this;
    }

    public SpeciesBuilder setMinTemperature(int minTemperature)
    {
        this.minTemperature = minTemperature;
        return this;
    }

    public SpeciesBuilder setMaxTemperature(int maxTemperature)
    {
        this.maxTemperature = maxTemperature;
        return this;
    }

    public SpeciesBuilder setDefaultHabitat(Habitat.Type defaultHabitat)
    {
        this.defaultHabitat = defaultHabitat;
        return this;
    }

    public SpeciesBuilder setNoiseBehavior(NoiseBehavior noiseBehavior)
    {
        this.noiseBehavior = noiseBehavior;
        return this;
    }

    public SpeciesBuilder setPregnancyDuration(int pregnancyDuration)
    {
        this.pregnancyDuration = pregnancyDuration;
        return this;
    }

    public SpeciesBuilder setLegs(int legs)
    {
        this.legs = legs;
        return this;
    }

    public Species createSpecies()
    {
        return new Species(name, speed, minTemperature, maxTemperature, defaultHabitat, noiseBehavior, pregnancyDuration, legs);
    }
}