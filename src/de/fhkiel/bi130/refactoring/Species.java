package de.fhkiel.bi130.refactoring;

/**
 * Storing information about the species
 *
 * Default Species are set in enum Species.Defaults
 */
public class Species
{
    public enum Defaults
    {
        BEAR(new SpeciesBuilder()
                .setName("bear")
                .setSpeed(50)
                .setMinTemperature(-20)
                .setMaxTemperature(50)
                .setDefaultHabitat(Habitat.Type.FOREST)
                .setNoiseBehavior(new SingleSoundNoiseBehavior()
                    .setNoise("Growl!"))
                .setPregnancyDuration(2)
                .setLegs(4)
                .createSpecies()),
        FOX(new SpeciesBuilder()
                .setName("fox")
                .setSpeed(50)
                .setMinTemperature(-20)
                .setMaxTemperature(50)
                .setDefaultHabitat(Habitat.Type.FOREST)
                .setNoiseBehavior(new MultiSoundNoiseBehavior()
                    .setNoise("Ring-ding-ding-ding-dingeringeding")
                    .setNoise("Joff-tchoff-tchoffo-tchoffo-tchoff")
                    .setNoise("Gering-ding-ding-ding-dingeringeding"))
                .setPregnancyDuration(2)
                .setLegs(4)
                .createSpecies()),
        CLOWNFISH(new SpeciesBuilder()
                .setName("clownfish")
                .setSpeed(24)
                .setMinTemperature(27)
                .setMaxTemperature(6)
                .setDefaultHabitat(Habitat.Type.SEA)
                .setNoiseBehavior(new SingleSoundNoiseBehavior()
                    .setNoise("blubb"))
                .createSpecies()),
        RATTLESNAKE(new SpeciesBuilder()
                .setName("rattlesnake")
                .setSpeed(26)
                .setMinTemperature(32)
                .setMaxTemperature(6)
                .setDefaultHabitat(Habitat.Type.DESERT)
                .setNoiseBehavior(new SingleSoundNoiseBehavior()
                    .setNoise("*rattle rattle* SssSSssSS"))
                .createSpecies()),
        HORSE( new SpeciesBuilder()
                .setName("horse")
                .setSpeed(40)
                .setMinTemperature(5)
                .setMaxTemperature(40)
                .setDefaultHabitat(Habitat.Type.PLAINS)
                .setNoiseBehavior(new SingleSoundNoiseBehavior()
                    .setNoise("neigh!"))
                .setLegs(4)
                .createSpecies()),
        SNAIL( new SpeciesBuilder()
                .setName("snail")
                .setSpeed(1)
                .setMinTemperature(10)
                .setMinTemperature(15)
                .setDefaultHabitat(Habitat.Type.FOREST)
                .setNoiseBehavior(new MultiSoundNoiseBehavior()
                    .setNoise("Omnomnom")
                    .setNoise("Nyamnyamnyam")
                    .setNoise("Snailsnailsnail")
                    .setNoise("I am bringing the bits from the internet in Germany!"))
                .createSpecies());


        private Species species;

        Defaults(Species species)
        {
            this.species = species;
        }

        public Species getDefault()
        {
            return new Species(species);
        }
    }

    private String name;
    private int pregnancyDuration;
    private int legs;
    private int speed;
    private String sound;
    private int minTemperature;
    private int maxTemperature;
    private Habitat.Type defaultHabitat;
    private NoiseBehavior noiseBehavior;

    public Species(String name, int speed, int minTemperature, int maxTemperature, Habitat.Type defaultHabitat, NoiseBehavior noiseBehavior, int pregnancyDuration, int legs)
    {
        this.name = name;
        this.pregnancyDuration = pregnancyDuration;
        this.legs = legs;
        this.speed = speed;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
        this.defaultHabitat = defaultHabitat;
        this.noiseBehavior = noiseBehavior;
    }

    public Species(Species species)
    {
        this.name = species.name;
        this.pregnancyDuration = species.pregnancyDuration;
        this.legs = species.legs;
        this.speed = species.speed;
        this.minTemperature = species.minTemperature;
        this.maxTemperature = species.maxTemperature;
        this.defaultHabitat = species.defaultHabitat;
        this.noiseBehavior = species.noiseBehavior;
    }

    public String makeNoise()
    {
        return sound;
    }

    public int getPregnancyDuration()
    {
        return pregnancyDuration;
    }

    public void setPregnancyDuration(int pregnancyDuration)
    {
        this.pregnancyDuration = pregnancyDuration;
    }

    public int getLegs()
    {
        return legs;
    }

    public void setLegs(int legs)
    {
        this.legs = legs;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getMinTemperature()
    {
        return minTemperature;
    }

    public void setMinTemperature(int minTemperature)
    {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature()
    {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature)
    {
        this.maxTemperature = maxTemperature;
    }

    public Habitat.Type getDefaultHabitat()
    {
        return defaultHabitat;
    }

    public void setDefaultHabitat(Habitat.Type defaultHabitat)
    {
        this.defaultHabitat = defaultHabitat;
    }

    public NoiseBehavior getNoiseBehavior()
    {
        return noiseBehavior;
    }

    public void setNoiseBehavior(NoiseBehavior noiseBehavior)
    {
        this.noiseBehavior = noiseBehavior;
    }

    @Override
    public String toString()
    {
        return this.getName();
    }
}
