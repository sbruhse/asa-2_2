package de.fhkiel.bi130.refactoring;

/**
 * Strategy for Animals which are doing just one sound
 */
public class SingleSoundNoiseBehavior extends NoiseBehavior
{

    private String noise;


    @Override
    public String toString()
    {
        return noise;
    }

    @Override
    public NoiseBehavior setNoise(String noise)
    {
        this.noise = noise;
        return this;
    }

}
