package de.fhkiel.bi130.refactoring;

/**
 * Storing information about the environment
 *
 * Default habitat types are stored in enum Habitat.Type
 */
public class Habitat
{
    public enum Type
    {
        FOREST(25),
        SEA(25),
        LAKE(25),
        DESERT(30),
        TUNDRA(-10),
        PLAINS(15);

        private int temperature;

        Type(int temperature)
        {
            this.temperature = temperature;
        }

        public int getDefaultTemperature()
        {
            return temperature;
        }

        public Habitat toHabitat()
        {
            return new Habitat(this, this.getDefaultTemperature());
        }

    }

    private final Type name;
    private int temperature;

    public Habitat(Type name, int temperature)
    {
        this.name = name;
        this.temperature = temperature;
    }

    public Type getName()
    {
        return name;
    }

    public int getTemperature()
    {
        return temperature;
    }

    public void setTemperature(int temperature)
    {
        this.temperature = temperature;
    }


}
