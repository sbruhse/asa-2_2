package de.fhkiel.bi130.refactoring;

public class Main
{

    public static void main(String[] args)
    {
        Zoo hanoverZoo = new Zoo("Hanover Zoo");

        hanoverZoo.addCompound(new Compound(new Habitat(Habitat.Type.LAKE, Habitat.Type.LAKE.getDefaultTemperature())));
        hanoverZoo.addCompound(new Compound(new Habitat(Habitat.Type.TUNDRA, Habitat.Type.TUNDRA.getDefaultTemperature())));


        Compound forestCompound = new Compound(new Habitat(Habitat.Type.FOREST, Habitat.Type.FOREST.getDefaultTemperature()));

        Animal winnieThePooh = new Animal(Species.Defaults.BEAR.getDefault(), "Winnie the Pooh");
        Animal fox = new Animal(Species.Defaults.FOX.getDefault());

        forestCompound.addAnimal(winnieThePooh);
        forestCompound.addAnimal(fox);
        forestCompound.addAnimal(new Animal(Species.Defaults.FOX.getDefault()));
        forestCompound.addAnimal(new Animal(Species.Defaults.FOX.getDefault(), "Joe"));


        System.out.println(winnieThePooh.getName() + (winnieThePooh.getAlive() ? " lebt" : " ist tot"));
        winnieThePooh.setAlive(false);
        fox.setAlive(false);
        System.out.println(winnieThePooh.getName() + (winnieThePooh.getAlive() ? " lebt" : " ist tot"));


        hanoverZoo.addCompound(forestCompound);


        Compound seaCompound = new Compound(new Habitat(Habitat.Type.SEA, Habitat.Type.SEA.getDefaultTemperature()));

        seaCompound.addAnimal(new Animal(Species.Defaults.CLOWNFISH.getDefault(), "Nemo"));
        seaCompound.addAnimal(new Animal(Species.Defaults.CLOWNFISH.getDefault(), "Marve"));

        hanoverZoo.addCompound(seaCompound);


        Compound desertCompound = new Compound(new Habitat(Habitat.Type.DESERT, Habitat.Type.DESERT.getDefaultTemperature()));

        desertCompound.addAnimal(new Animal(Species.Defaults.RATTLESNAKE.getDefault()));
        desertCompound.addAnimal(new Animal(Species.Defaults.RATTLESNAKE.getDefault(), "Kaa"));
        desertCompound.addAnimal(new Animal(Species.Defaults.CLOWNFISH.getDefault(), "Nemo's Mother"));

        hanoverZoo.addCompound(desertCompound);

        hanoverZoo.checkCompounds();

        System.out.println(hanoverZoo.getName() + " has " + hanoverZoo.countAnimals() + " animals.");
        System.out.println("\"Fun\" fact: All together, they have " + hanoverZoo.countAnimalsLegs() + " legs.");


        hanoverZoo.printZooCompoundsInfo();

        hanoverZoo.setTemperatureInAllHabitatCompounds(35, Habitat.Type.DESERT);
        hanoverZoo.setTemperatureInAllHabitatCompounds(-5, Habitat.Type.SEA);
        hanoverZoo.setTemperatureInAllHabitatCompounds(35, Habitat.Type.FOREST);

        System.out.println(seaCompound.getAnimals().get(0).getSpecies().getNoiseBehavior());
        System.out.println(forestCompound.getAnimals().get(1).getSpecies().getNoiseBehavior());
        System.out.println(forestCompound.getAnimals().get(1).getSpecies().getNoiseBehavior());
        System.out.println(forestCompound.getAnimals().get(1).getSpecies().getNoiseBehavior());

        Compound plainsCompound = new Compound(new Habitat(Habitat.Type.PLAINS, Habitat.Type.PLAINS.getDefaultTemperature()));

        plainsCompound.addAnimal(new Animal(Species.Defaults.HORSE.getDefault(), "Lasagne"));
        plainsCompound.addAnimal(new Animal(Species.Defaults.HORSE.getDefault(), "Salami"));

        hanoverZoo.addCompound(plainsCompound);

        System.out.println(hanoverZoo.getName() + " has " + hanoverZoo.countAnimals() + " animals.");

        for (int i = 0; i < 100; i++)
        {
            hanoverZoo.getCompounds(Habitat.Type.FOREST).get(0).addAnimal(new Animal(Species.Defaults.SNAIL.getDefault()));
        }

        System.out.println(hanoverZoo.getName() + " has " + hanoverZoo.countAnimals() + " animals.");

        hanoverZoo.getCompounds().get(0).getAnimals().forEach(animal -> System.out.println(animal.getSpecies()+ " makes \"" + animal.getSpecies().getNoiseBehavior() + "\""));
    }

}
