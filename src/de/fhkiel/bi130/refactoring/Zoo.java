package de.fhkiel.bi130.refactoring;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Zoo class which is storing the compounds
 */
public class Zoo
{
    private String name;

    private Map<Habitat.Type, ArrayList<Compound>> compounds = new EnumMap<>(Habitat.Type.class);

    Zoo(String name)
    {
        this.name = name;
    }

    public void checkCompounds()
    {
        for (Compound c : getCompounds())
        {
            c.checkCompound();
        }
    }


    public List<Compound> getCompounds()
    {

        return compounds.values().stream().flatMap(List::stream).collect(Collectors.toList());

    }

    public void addCompound(Compound compound)
    {
        if (!compounds.containsKey(compound.getHabitat().getName()))
        {
            compounds.put(compound.getHabitat().getName(), new ArrayList<>());
        }

        compounds.get(compound.getHabitat().getName()).add(compound);
    }

    public int countAnimals()
    {
        int count = 0;
        for (Compound c : getCompounds())
        {
            count = count + c.countAnimals();
        }
        return count;
    }

    public int countAnimalsLegs()
    {
        int legSum = 0;
        for (Compound c : getCompounds())
        {
            legSum += c.countAnimalsLegs();
        }
        return legSum;
    }

    public void printZooCompoundsInfo()
    {
        for (Compound c : getCompounds()) c.printCompoundInfo();
    }

    public void setTemperatureInAllHabitatCompounds(int temperature, Habitat.Type type)
    {
        for (Compound compound : getCompounds(type))
        {
            compound.getHabitat().setTemperature(temperature);
        }
    }

    public String getName()
    {
        return name;
    }

    public List<Compound> getCompounds(Habitat.Type type)
    {
        return new ArrayList<>(compounds.get(type));
    }

}
